/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 01:11:52 by smonroe           #+#    #+#             */
/*   Updated: 2018/05/26 18:46:43 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft/libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdio.h>

void	print_error(int m);
char	*read_file(char *file);
int		check_line(char *s, int i);
void	validate_bloc(char *s, int i, int t);
void	validate_file(char *s);
char	***make_cube(char *s);
char	***fill_cube(char *s, char ***cube);
char	***check_cube(char ***cube);
int		check_surround(char ***cube, int z, int y, int x);
char	**zero_piece(char **piece);
char	**move_up(char **piece);
char	**move_left(char **piece);
char	**make_grid(int size);
int		place_grid(char **grid, int *vals, char **piece);
void	undo_place(char **grid, int t);
int		grid_count(char **grid);
int		get_max(char **bloc);
void	print_grid(char **bloc);
void	free_grid(char **grid);
void	free_cube(char ***cube);
void	call_back(char **grid, char ***cube);
int		backtrack(char **grid, char ***cube, int *vals);

#endif
