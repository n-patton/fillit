/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 14:12:24 by smonroe           #+#    #+#             */
/*   Updated: 2018/06/05 22:31:15 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	***make_cube(char *s)
{
	char	***cube;
	int		y;
	int		z;
	int		t;

	y = 0;
	z = 0;
	t = (ft_strlen(s) + 1) / 21;
	if (!(cube = (char ***)malloc(sizeof(char **) * t + 1)))
		return (NULL);
	while (z < t)
	{
		if (!(cube[z] = (char **)malloc(sizeof(char *) * 5)))
			return (NULL);
		y = 0;
		while (y < 4)
			if (!(cube[z][y++] = (char *)malloc(sizeof(char) * 5)))
				return (NULL);
		if (!(cube[z++][y] = (char *)malloc(sizeof(char) * 1)))
			return (NULL);
	}
	if (!(cube[z] = (char **)malloc(sizeof(char *) * 1)))
		return (NULL);
	cube = fill_cube(s, cube);
	return (cube);
}

char	***fill_cube(char *s, char ***cube)
{
	int	t;
	int	z;
	int	y;
	int	x;

	t = (ft_strlen(s) + 1) / 21;
	z = -1;
	while (++z < t)
	{
		y = 0;
		while (y < 4)
		{
			x = 0;
			while (x < 4)
				cube[z][y][x++] = *s++;
			s += 1;
			cube[z][y++][x] = 0;
		}
		s += 1;
		cube[z][y] = 0;
		cube[z] = zero_piece(cube[z]);
	}
	cube[z] = 0;
	return (cube);
}

char	***check_cube(char ***cube)
{
	int	z;
	int	y;
	int	x;
	int	q;

	z = -1;
	while (cube[++z])
	{
		q = 0;
		y = -1;
		while (cube[z][++y])
		{
			x = -1;
			while (cube[z][y][++x])
				if (cube[z][y][x] == '#')
					q += check_surround(cube, z, y, x);
		}
		if (q < 3)
			print_error(1);
	}
	return (cube);
}

int		check_surround(char ***cube, int z, int y, int x)
{
	int	g;

	g = 0;
	if (x < 3)
		if (cube[z][y][x + 1] != '.')
			g++;
	if (y < 3)
		if (cube[z][y + 1][x] != '.')
			g++;
	cube[z][y][x] = 'A' + z;
	return (g);
}

void	free_cube(char ***cube)
{
	int	y;
	int	z;

	z = -1;
	while (cube[++z])
	{
		y = -1;
		while (cube[z][++y])
			free(cube[z][y]);
	}
	free(cube);
}
