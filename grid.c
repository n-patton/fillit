/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 00:06:08 by smonroe           #+#    #+#             */
/*   Updated: 2018/06/05 22:29:38 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**make_grid(int size)
{
	char	**grid;
	int		x;

	if (!(grid = (char **)malloc(sizeof(char *) * size + 1)))
		return (NULL);
	x = -1;
	while (++x < size)
	{
		if (!(grid[x] = (char *)malloc(sizeof(char) * size + 1)))
			return (NULL);
		grid[x] = (char *)ft_memset(grid[x], '.', size);
		grid[x][size] = 0;
	}
	grid[size] = NULL;
	return (grid);
}

int		place_grid(char **grid, int *vals, char **piece)
{
	int	x;
	int	y;

	y = -1;
	while (piece[++y])
	{
		x = -1;
		while (piece[y][++x])
		{
			if (piece[y][x] != '.' && grid[y + vals[3]][x + vals[4]] == '.')
			{
				if (y + vals[3] > vals[0] - 1 || x + vals[4] > vals[0] - 1)
					return (0);
				grid[y + vals[3]][x + vals[4]] = piece[y][x];
			}
			else if (piece[y][x] != '.' &&
					grid[y + vals[3]][x + vals[4]] != '.')
				return (0);
		}
	}
	return (1);
}

void	undo_place(char **grid, int t)
{
	int	x;
	int	y;

	y = -1;
	while (grid[++y])
	{
		x = -1;
		while (grid[y][++x])
			if (grid[y][x] == 'A' + t)
				grid[y][x] = '.';
	}
}

int		grid_count(char **grid)
{
	int	x;
	int	y;
	int	c;

	c = 0;
	y = -1;
	while (grid[++y])
	{
		x = -1;
		while (grid[y][++x])
			if (grid[y][x] != '.')
				c++;
	}
	return (c);
}

void	print_grid(char **bloc)
{
	int	x;
	int	y;
	int	max;

	max = get_max(bloc);
	y = -1;
	while (++y < max + 1)
	{
		x = -1;
		while (++x < max + 1)
			ft_putchar(bloc[y][x]);
		ft_putchar('\n');
	}
}
